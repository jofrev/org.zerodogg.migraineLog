#!/bin/bash
usage ()
{
    echo "USAGE: $0 ANDROID_LANGUAGE APPSTORE_LANGUAGE"
    echo "Example: $0 no-NO no"
    exit $1
}
ANDROID_LANGUAGE="$1"
APPSTORE_LANGUAGE="$2"
FASTLANE="../../fastlane/metadata/android/"
if [ "$ANDROID_LANGUAGE" == "" ] || [ "$APPSTORE_LANGUAGE" == "" ]; then
    usage 1
fi
if [ ! -e ../../fastlane/metadata/android/$ANDROID_LANGUAGE ]; then
    echo "$1: android language not found"
    echo ""
    usage 2
fi
mkdir -p "$APPSTORE_LANGUAGE"
cd "$APPSTORE_LANGUAGE"
FASTLANE="../$FASTLANE"
ln -fs "$FASTLANE/$ANDROID_LANGUAGE/full_description.txt" "description.txt"
ln -fs "$FASTLANE/$ANDROID_LANGUAGE/title.txt" "name.txt"
for f in ../en-GB/*url*txt; do
    BNAME="$(basename "$f")"
    if [ -e "$BNAME" ]; then
        if [ "$(cat "$BNAME")" == "" ]; then
            rm -f "$BNAME"
        fi
    fi
    if [ ! -e "$BNAME" ]; then
        ln -sf "$f" .
    fi
done
